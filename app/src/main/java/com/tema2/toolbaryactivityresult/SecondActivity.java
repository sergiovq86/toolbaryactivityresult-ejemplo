package com.tema2.toolbaryactivityresult;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        final TextInputEditText editText = findViewById(R.id.et_text);
        Button bt_send = findViewById(R.id.btn_send);

        bt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if (text.isEmpty()) {
                    Toast.makeText(SecondActivity.this, "No has escrito nada", Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent();
                    i.putExtra("texto", text);
                    setResult(RESULT_OK, i);
                    finish();
                }
            }
        });

        ActionBar actionBar = getSupportActionBar();
        //comprueba que el tema tiene actionBar
        if (actionBar != null) {
            actionBar.setTitle("Second Activity");
            actionBar.setSubtitle("Hola");
            //activar botón atrás
            actionBar.setDisplayHomeAsUpEnabled(true);

            //Sustituir botón atrás por icono
//            actionBar.setHomeAsUpIndicator(R.drawable.ic_battery_charging_80_black_24dp);

            //poner un icono
//            actionBar.setIcon(R.mipmap.ic_launcher);
//            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);


        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
