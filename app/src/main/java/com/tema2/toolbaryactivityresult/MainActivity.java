package com.tema2.toolbaryactivityresult;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 5;

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.tv_maintext);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("MyApp");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.nav_alert:
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("info", "Hola");
                startActivityForResult(intent, REQUEST_CODE);
                return true;

            case R.id.nav_battery:
                Toast.makeText(this, "Battery level 58%", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.nav_cloud:
                Toast.makeText(this, "Upload pushed!!", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.nav_info:
                Toast.makeText(this, "Info menu pushed", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String texto = data.getStringExtra("texto");
                    textView.setText(texto);
                }
            } else if (resultCode == RESULT_CANCELED) {
                textView.setText("Operación cancelada");
            }
        }
    }
}
